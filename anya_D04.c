// soal shift modul 4 kelompok D04 2022

#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <ctype.h>
#include <time.h>

#define maxSize 1024

// variabel global
char dirpath[maxSize];
char *key_for_vigenere = "INNUGANTENG";
char *animeku = "Animeku_";
char *ian = "IAN_";
char *nds = "nam_do-saq_";
int x = 0;

// . dan /
int split_ext_id(char *path)
{
  int flag = 0;
  for (int i = strlen(path) - 1; i >= 0; i--)
  {
    if (path[i] == '.')
    {
      if (flag == 1)
        return i;
      else
        flag = 1;
    }
  }
  return strlen(path);
}

int ext_id(char *path)
{
  for (int i = strlen(path) - 1; i >= 0; i--)
  {
    if (path[i] == '.')
      return i;
  }
  return strlen(path);
}

int slash_id(char *path, int hasil)
{
  for (int i = 0; i < strlen(path); i++)
  {
    if (path[i] == '/')
      return i + 1;
  }
  return hasil;
}

// fungsi untuk Wibu.log
void wibu_log(char *nama, const char *from, const char *to)
{
  char wibuLogMsg[maxSize];

  FILE *file;
  file = fopen("/home/lintang/wibu.log", "a");
  sprintf(wibuLogMsg, "%s %s/%s --> %s/%s\n", nama, dirpath, from, dirpath, to);

  fputs(wibuLogMsg, file);
  fclose(file);
  return;
}

// void wibu_log(char *nama, const char *from, const char *to)
// {
//   char wibuLogMsg[maxSize];

//   FILE *file;
//   file = fopen("/home/lintang/Wibu.log", "a");

//   if (strcmp(nama, "RENAME") == 0 && strstr(to, "Animeku_") != NULL)
//     sprintf(wibuLogMsg, "%s terenkripsi \t %s --> %s \n", nama, from, to);
//   else if (strcmp(nama, "RENAME") == 0 && strstr(to, "Animeku_") == NULL)
//     sprintf(wibuLogMsg, "%s terdecode \t %s --> %s \n", nama, from, to);

//   fprintf(file, "%s", wibuLogMsg);
//   fclose(file);
// }

// fungsi untuk atbash cipher mix rot 13
void atbash_x_rot13_encrypt_func(char *filename)
{
  if (!strcmp(filename, ".") || !strcmp(filename, ".."))
    return;

  int endID = split_ext_id(filename);
  if (endID == strlen(filename))
    endID = ext_id(filename);
  int startID = slash_id(filename, 0);

  char file_origin[maxSize];
  for (int i = startID; i < endID; ++i)
  {
    file_origin[i] = filename[i];
    if (filename[i] != '/' && isalpha(filename[i]))
    {
      if (filename[i] >= 'A' && filename[i] <= 'Z')
      {
        if (!((filename[i] >= 0 && filename[i] < 65) || (filename[i] > 90 && filename[i] < 97) || (filename[i] > 122 && filename[i] <= 127)))
        {
          if (filename[i] >= 'A' && filename[i] <= 'Z')
            filename[i] = 'Z' + 'A' - filename[i];
          if (filename[i] >= 'a' && filename[i] <= 'z')
            filename[i] = 'z' + 'a' - filename[i];
        }
      }

      else
      {
        if ((filename[i] >= 97 && filename[i] <= 122) || (filename[i] >= 65 && filename[i] <= 90))
        {
          if (filename[i] > 109 || (filename[i] > 77 && filename[i] < 91))
            filename[i] -= 13;
          else
            filename[i] += 13;
        }
      }
    }
  }
  wibu_log("RENAME terenkripsi", file_origin, filename);
}

void atbash_x_rot13_decrypt_func(char *filename)
{
  if (!strcmp(filename, ".") || !strcmp(filename, ".."))
    return;

  int endID = split_ext_id(filename);
  if (endID == strlen(filename))
    endID = ext_id(filename);
  int startID = slash_id(filename, endID);

  char file_origin[maxSize];
  for (int i = startID; i < endID; ++i)
  {
    file_origin[i] = filename[i];
    if (filename[i] != '/' && isalpha(filename[i]))
    {
      if (filename[i] >= 'A' && filename[i] <= 'Z')
      {
        if (!((filename[i] >= 0 && filename[i] < 65) || (filename[i] > 90 && filename[i] < 97) || (filename[i] > 122 && filename[i] <= 127)))
        {
          if (filename[i] >= 'A' && filename[i] <= 'Z')
            filename[i] = 'Z' + 'A' - filename[i];
          if (filename[i] >= 'a' && filename[i] <= 'z')
            filename[i] = 'z' + 'a' - filename[i];
        }
      }

      else
      {
        if ((filename[i] >= 97 && filename[i] <= 122) || (filename[i] >= 65 && filename[i] <= 90))
        {
          if (filename[i] > 109 || (filename[i] > 77 && filename[i] < 91))
            filename[i] -= 13;
          else
            filename[i] += 13;
        }
      }
    }
  }
  wibu_log("RENAME terdecode", file_origin, filename);
}

// fungsi untuk vigenere cipher
void vigenere_encrypt_func(char *src)
{
  if (!strcmp(src, ".") || !strcmp(src, ".."))
    return;

  int endID = split_ext_id(src);
  int startID = slash_id(src, 0);

  for (int i = startID; i < endID; i++)
  {
    if (src[i] != '/' && isalpha(src[i]))
    {
      char temp1 = src[i];
      char temp2 = key_for_vigenere[(i - startID) % strlen(key_for_vigenere)];
      if (isupper(src[i]))
      {
        temp1 -= 'A';
        temp2 -= 'A';
        temp1 = (temp1 + temp2) % 26;
        temp1 += 'A';
      }
      else
      {
        temp1 -= 'a';
        temp2 = tolower(temp2) - 'a';
        temp1 = (temp1 + temp2) % 26;
        temp1 += 'a';
      }
      src[i] = temp1;
    }
  }
}

void vigenere_decrypt_func(char *src)
{
  if (!strcmp(src, ".") || !strcmp(src, ".."))
    return;

  int endID = split_ext_id(src);
  int startID = slash_id(src, endID);

  for (int i = startID; i < endID; i++)
  {
    if (src[i] != '/' && isalpha(src[i]))
    {
      char temp1 = src[i];
      char temp2 = key_for_vigenere[(i - startID) % strlen(key_for_vigenere)];
      if (isupper(src[i]))
      {
        temp1 -= 'A';
        temp2 -= 'A';
        temp1 = (temp1 - temp2 + 26) % 26; // Vigenere cipher
        temp1 += 'A';
      }
      else
      {
        temp1 -= 'a';
        temp2 = tolower(temp2) - 'a';
        temp1 = (temp1 - temp2 + 26) % 26; // Vigenere cipher
        temp1 += 'a';
      }
      src[i] = temp1;
    }
  }
}

// fungsi untuk hayolongapain_d04.log
void hayolongapain_d04_log(char *nama, char *fpath)
{
  // [Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]
  //   Contoh :
  // WARNING::21042022-12:02:42::RMDIR::/RichBrian
  // INFO::21042022-12:01:20::RENAME::/Song::/IAN_Song
  // INFO::21042022-12:06:41::MKDIR::/BillieEilish

  time_t rawtime;
  struct tm *timeinfo;
  time(&rawtime);
  timeinfo = localtime(&rawtime);

  char hayolongapainLogMsg[maxSize];

  FILE *file;
  file = fopen("/home/lintang/hayolongapain_d04.log", "a");

  if (strcmp(nama, "UNLINK") == 0 || strcmp(nama, "RMDIR") == 0)
    sprintf(hayolongapainLogMsg, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, fpath);
  else
    sprintf(hayolongapainLogMsg, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, fpath);

  fputs(hayolongapainLogMsg, file);
  fclose(file);
  return;
}

void hayolongapain_d04_log_b(char *nama, const char *from, const char *to)
{
  time_t rawtime;
  struct tm *timeinfo;
  time(&rawtime);
  timeinfo = localtime(&rawtime);

  char hayolongapainLogMsg[maxSize];

  FILE *file;
  file = fopen("/home/lintang/hayolongapain_d04.log", "a");

  if (strcmp(nama, "UNLINK") == 0 || strcmp(nama, "RMDIR") == 0)
    sprintf(hayolongapainLogMsg, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, from, to);
  else
    sprintf(hayolongapainLogMsg, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, from, to);

  fputs(hayolongapainLogMsg, file);
  fclose(file);
  return;
}

// enkripsi dan dekripsi
void encryption_sec(char *fpath)
{
  chdir(fpath);
  DIR *dp;
  struct dirent *de;

  dp = opendir(".");
  if (dp == NULL)
    return;
  struct stat st;
  char dirPath[1000];
  char filePath[1000];

  while ((de = readdir(dp)) != NULL)
  {
    printf("dirname %s\n", de->d_name);
    printf("%s/%s\n", fpath, de->d_name);
    if (stat(de->d_name, &st) < 0)
      ;
    else if (S_ISDIR(st.st_mode))
    {
      if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
        continue;
      sprintf(dirPath, "%s/%s", fpath, de->d_name);
      encryption_sec(dirPath);
      printf("dirpath %s\n", dirPath);
    }
    else
    {
      sprintf(filePath, "%s/%s", fpath, de->d_name);
      FILE *input = fopen(filePath, "r");
      if (input == NULL)
        return;
      int bytes_read, count = 0;
      void *buffer = malloc(1024);
      while ((bytes_read = fread(buffer, 1, 1024, input)) > 0)
      {
        char newFilePath[1000];
        sprintf(newFilePath, "%s.%04d", filePath, count);
        count++;
        FILE *output = fopen(newFilePath, "w+");
        if (output == NULL)
          return;
        fwrite(buffer, 1, bytes_read, output);
        fclose(output);
        memset(buffer, 0, 1024);
      }
      fclose(input);
      printf("filepath %s\n", filePath);
      remove(filePath);
    }
  }
  closedir(dp);
}

void decryption_sec(char *dir)
{
  chdir(dir);
  DIR *dp;
  struct dirent *de;
  struct stat st;
  dp = opendir(".");
  if (dp == NULL)
    return;

  char dirPath[1000];
  char filePath[1000];

  while ((de = readdir(dp)) != NULL)
  {
    if (stat(de->d_name, &st) < 0)
      ;
    else if (S_ISDIR(st.st_mode))
    {
      if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
        continue;
      sprintf(dirPath, "%s/%s", dir, de->d_name);
      decryption_sec(dirPath);
    }
    else
    {
      sprintf(filePath, "%s/%s", dir, de->d_name);
      filePath[strlen(filePath) - 5] = '\0';
      FILE *check = fopen(filePath, "r");
      if (check != NULL)
        return;
      FILE *file = fopen(filePath, "w");
      int count = 0;
      char topath[1000];
      sprintf(topath, "%s.%04d", filePath, count);
      void *buffer = malloc(1024);
      while (1)
      {
        FILE *op = fopen(topath, "rb");
        if (op == NULL)
          break;
        size_t readSize = fread(buffer, 1, 1024, op);
        fwrite(buffer, 1, readSize, file);
        fclose(op);
        remove(topath);
        count++;
        sprintf(topath, "%s.%04d", filePath, count);
      }
      free(buffer);
      fclose(file);
    }
  }
  closedir(dp);
}

// char *findPath(const char *path)
// {
//   char file_path[2 * maxSize];
//   bool flag, toMirror = 0;

//   char *strMirror;
//   if (strcmp(path, "/"))
//   {
//     strMirror = strstr(path, "/Animeku_");

//     if (strMirror)
//     {
//       toMirror = 1;
//       strMirror++;
//     }
//   }

//   if (strcmp(path, "/") == 0)
//   {
//     sprintf(file_path, "%s", dirpath);
//   }
//   else if (toMirror)
//   {
//     char pathAsli[maxSize] = "";
//     char t[maxSize];

//     strncpy(pathAsli, path, strlen(path) - strlen(strMirror));
//     strcpy(t, strMirror);

//     char *selectedFile;
//     char *rest = t;

//     flag = 0;

//     while ((selectedFile = strtok_r(rest, "/", &rest)))
//     {
//       if (!flag)
//       {
//         strcat(pathAsli, selectedFile);
//         flag = 1;
//         continue;
//       }

//       char cekTipe[2 * maxSize];
//       sprintf(cekTipe, "%s/%s", pathAsli, selectedFile);
//       strcat(pathAsli, "/");

//       if (strlen(cekTipe) == strlen(path))
//       {
//         char pathFolder[2 * maxSize];
//         sprintf(pathFolder, "%s%s%s", dirpath, pathAsli, selectedFile);

//         DIR *dp = opendir(pathFolder);
//         if (!dp)
//         {
//           char *ext;
//           ext = strrchr(selectedFile, '.');

//           char fileName[maxSize] = "";
//           if (ext)
//           {
//             strncpy(fileName, selectedFile, strlen(selectedFile) - strlen(ext));
//             sprintf(fileName, "%s%s", atbash_x_rot13(fileName), ext);
//           }
//           else if (toMirror)
//           {
//             strcpy(fileName, atbash_x_rot13(selectedFile));
//           }

//           printf("%s\n", fileName);
//           strcat(pathAsli, fileName);
//         }
//         else
//         {
//           strcat(pathAsli, atbash_x_rot13(selectedFile));
//         }
//       }
//       else
//       {
//         strcat(pathAsli, atbash_x_rot13(selectedFile));
//       }
//     }
//     sprintf(file_path, "%s%s", dirpath, pathAsli);
//   }
//   else
//     sprintf(file_path, "%s%s", dirpath, path);

//   char *file_path_to_return = file_path;
//   return file_path_to_return;
// }

// biner dan desimal
void getBin(char *fname, char *bin, char *lowercase)
{
  int ID_end = ext_id(fname);
  int ID_start = slash_id(fname, 0);
  int i;

  for (i = ID_start; i < ID_end; i++)
  {
    if (isupper(fname[i]))
    {
      bin[i] = '1';
      lowercase[i] = fname[i] + 32;
    }
    else
    {
      bin[i] = '0';
      lowercase[i] = fname[i];
    }
  }
  bin[ID_end] = '\0';

  for (; i < strlen(fname); i++)
  {
    lowercase[i] = fname[i];
  }
  lowercase[i] = '\0';
}

int BinToDec(char *bin)
{
  int temp = 1, hasil = 0;
  for (int i = strlen(bin) - 1; i >= 0; i--)
  {
    if (bin[i] == '1')
      hasil += temp;
    temp *= 2;
  }
  return hasil;
}

void encryptBin(char *fpath)
{
  chdir(fpath);
  DIR *dp;
  struct dirent *dir;
  struct stat st;
  dp = opendir(".");
  if (dp == NULL)
    return;

  char dirPath[1000];
  char filePath[1000];
  char filePathBin[1000];

  while ((dir = readdir(dp)) != NULL)
  {
    if (stat(dir->d_name, &st) < 0)
      ;
    else if (S_ISDIR(st.st_mode))
    {
      if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0)
        continue;
      sprintf(dirPath, "%s/%s", fpath, dir->d_name);
      encryptBin(dirPath);
    }
    else
    {
      sprintf(filePath, "%s/%s", fpath, dir->d_name);
      char bin[1000], low_case[1000];
      getBin(dir->d_name, bin, low_case);
      int dec = BinToDec(bin);
      sprintf(filePathBin, "%s/%s.%d", fpath, low_case, dec);
      rename(filePath, filePathBin);
    }
  }
  closedir(dp);
}

int convertDec(char *ext)
{
  int dec = 0, kali = 1;
  for (int i = strlen(ext) - 1; i >= 0; i--)
  {
    dec += (ext[i] - '0') * kali;
    kali *= 10;
  }
  return dec;
}

void DecToBin(int dec, char *bin, int len)
{
  int index = 0;
  while (dec)
  {
    if (dec & 1)
      bin[index] = '1';
    else
      bin[index] = '0';
    index++;
    dec /= 2;
  }
  while (index < len)
  {
    bin[index] = '0';
    index++;
  }
  bin[index] = '\0';

  for (int i = 0; i < index / 2; i++)
  {
    char tmp = bin[i];
    bin[i] = bin[index - 1 - i];
    bin[index - 1 - i] = tmp;
  }
}

void getDec(char *fname, char *bin, char *normalcase)
{
  int ID_end = ext_id(fname);
  int ID_start = slash_id(fname, 0);
  int i;

  for (i = ID_start; i < ID_end; i++)
  {
    if (bin[i - ID_start] == '1')
      normalcase[i - ID_start] = fname[i] - 32;
    else
      normalcase[i - ID_start] = fname[i];
  }

  for (; i < strlen(fname); i++)
  {
    normalcase[i - ID_start] = fname[i];
  }
  normalcase[i - ID_start] = '\0';
}

void decryptBin(char *fpath)
{
  chdir(fpath);
  DIR *dp;
  struct dirent *dir;
  struct stat st;
  dp = opendir(".");
  if (dp == NULL)
    return;

  char dirPath[1000];
  char filePath[1000];
  char filePathDec[1000];

  while ((dir = readdir(dp)) != NULL)
  {
    if (stat(dir->d_name, &st) < 0)
      ;
    else if (S_ISDIR(st.st_mode))
    {
      if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0)
        continue;
      sprintf(dirPath, "%s/%s", fpath, dir->d_name);
      decryptBin(dirPath);
    }
    else
    {
      sprintf(filePath, "%s/%s", fpath, dir->d_name);
      char fname[1000], bin[1000], norm_case[1000], clearPath[1000];

      strcpy(fname, dir->d_name);
      char *ext = strrchr(fname, '.');
      int dec = convertDec(ext + 1);
      for (int i = 0; i < strlen(fname) - strlen(ext); i++)
        clearPath[i] = fname[i];

      char *ext2 = strrchr(clearPath, '.');
      DecToBin(dec, bin, strlen(clearPath) - strlen(ext2));
      getDec(clearPath, bin, norm_case);
      sprintf(filePathDec, "%s/%s", fpath, norm_case);
      rename(filePath, filePathDec);
    }
  }
  closedir(dp);
}

// fungsi-fungsi operasi
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  char file_path[maxSize];

  char *a = strstr(path, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);

  char *b = strstr(path, ian);
  if (b != NULL)
    vigenere_decrypt_func(b);

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(file_path, "%s", path);
  }
  else
    sprintf(file_path, "%s%s", dirpath, path);

  int res = 0;
  DIR *dp;
  struct dirent *de;

  (void)offset;
  (void)fi;

  dp = opendir(file_path);
  if (dp == NULL)
    return -errno;

  while ((de = readdir(dp)) != NULL)
  {
    struct stat st;
    memset(&st, 0, sizeof(st));
    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;

    if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
    {
      res = (filler(buf, de->d_name, &st, 0));
    }

    if (a != NULL)
      atbash_x_rot13_encrypt_func(de->d_name);
    if (b != NULL)
      vigenere_encrypt_func(de->d_name);

    res = (filler(buf, de->d_name, &st, 0));

    if (res != 0)
      break;
  }
  closedir(dp);
  return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  char file_path[maxSize];

  char *a = strstr(path, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);
  char *b = strstr(path, ian);
  if (b != NULL)
    vigenere_decrypt_func(b);

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(file_path, "%s", path);
  }
  else
    sprintf(file_path, "%s%s", dirpath, path);

  int fd = 0;
  int res = 0;

  (void)fi;
  fd = open(file_path, O_RDONLY);
  if (fd == -1)
    return -errno;

  res = pread(fd, buf, size, offset);
  if (res == -1)
    res = -errno;

  close(fd);
  return res;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
  char file_path[maxSize];

  char *a = strstr(path, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);

  char *b = strstr(path, ian);
  if (b != NULL)
    vigenere_decrypt_func(b);

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(file_path, "%s", path);
  }
  else
    sprintf(file_path, "%s%s", dirpath, path);

  int res;
  res = lstat(file_path, stbuf);
  if (res == -1)
    return -errno;

  return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)

{
  char file_path[maxSize];

  char *a = strstr(path, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);

  char *b = strstr(path, ian);
  if (b != NULL)
    vigenere_decrypt_func(b);

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(file_path, "%s", path);
  }
  else
    sprintf(file_path, "%s%s", dirpath, path);

  int res;
  res = mkdir(file_path, mode);
  if (res == -1)
    return -errno;

  // tulis ke hayolongapain_d04.log
  hayolongapain_d04_log("MKDIR", file_path);

  return 0;
}

static int xmp_rename(const char *from, const char *to)
{
  int res;
  char fromDir[maxSize], toDir[maxSize];

  char *a = strstr(to, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);

  char *b = strstr(from, ian);
  if (b != NULL)
    vigenere_decrypt_func(b);

  char *c = strstr(to, ian);
  if (c != NULL)
  {
    vigenere_encrypt_func(c);
  }

  // char *str_to = strstr(to, "Animeku_");
  // if (str_to)
  //   atbash_x_rot13(str_to);

  // char *str_from = strstr(from, "Animeku_");
  // if (str_from)
  //   atbash_x_rot13(str_from);

  sprintf(fromDir, "%s%s", dirpath, from);
  sprintf(toDir, "%s%s", dirpath, to);

  res = rename(fromDir, toDir);
  if (res == -1)
    return -errno;

  // tulis ke hayolongapain_d04.log yang kedua
  hayolongapain_d04_log_b("RENAME", fromDir, toDir);

  if (c != NULL)
  {
    encryption_sec(toDir);
    hayolongapain_d04_log_b("ENCRYPT2", from, to);
  }
  if (b != NULL && c == NULL)
  {
    decryption_sec(toDir);
    hayolongapain_d04_log_b("DECRYPT2", from, to);
  }

  if (strstr(to, nds) != NULL)
  {
    encryptBin(toDir);
    hayolongapain_d04_log_b("ENCRYPT3", from, to);
  }

  if (strstr(from, nds) != NULL && strstr(to, nds) == NULL)
  {
    decryptBin(toDir);
    hayolongapain_d04_log_b("DECRYPT3", from, to);
  }

  return 0;
}

static int xmp_unlink(const char *path)
{
  int res;
  char file_path[maxSize];

  char *a = strstr(path, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);
  char *b = strstr(path, ian);
  if (b != NULL)
    vigenere_decrypt_func(b);

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(file_path, "%s", path);
  }
  else
    sprintf(file_path, "%s%s", dirpath, path);

  res = unlink(file_path);
  if (res == -1)
    return -errno;

  // tulis ke hayolongapain_d04.log
  hayolongapain_d04_log("UNLINK", file_path);

  return 0;
}

static int xmp_rmdir(const char *path)
{
  int res;
  char file_path[maxSize];

  char *a = strstr(path, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);
  char *b = strstr(path, ian);
  if (b != NULL)
    vigenere_decrypt_func(b);

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(file_path, "%s", path);
  }
  else
    sprintf(file_path, "%s%s", dirpath, path);

  res = rmdir(file_path);
  if (res == -1)
    return -errno;

  // tulis ke hayolongapain_d04.log
  hayolongapain_d04_log("RMDIR", file_path);

  return 0;
}

static int xmp_chmod(const char *path, mode_t mode)
{
  int res;
  char file_path[maxSize];

  char *a = strstr(path, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);

  char *b = strstr(path, ian);
  if (b != NULL)
    vigenere_decrypt_func(b);

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(file_path, "%s", path);
  }
  else
    sprintf(file_path, "%s%s", dirpath, path);

  res = chmod(file_path, mode);
  if (res == -1)
    return -errno;

  // tulis ke hayolongapain_d04.log
  hayolongapain_d04_log("CHMOD", file_path);

  return 0;
}

static int xmp_chown(const char *path, uid_t uid, gid_t gid)
{
  int res;
  char file_path[maxSize];

  char *a = strstr(path, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);

  char *b = strstr(path, ian);
  if (b != NULL)
    vigenere_decrypt_func(b);

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(file_path, "%s", path);
  }
  else
    sprintf(file_path, "%s%s", dirpath, path);

  res = lchown(file_path, uid, gid);
  if (res == -1)
    return -errno;

  // tulis ke hayolongapain_d04.log
  hayolongapain_d04_log("CHOWN", file_path);

  return 0;
}

static int xmp_truncate(const char *path, off_t size)
{
  int res;
  char file_path[maxSize];

  char *a = strstr(path, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);

  char *b = strstr(path, ian);
  if (b != NULL)
  {
    vigenere_decrypt_func(b);
    atbash_x_rot13_decrypt_func(b);
  }

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(file_path, "%s", path);
  }
  else
    sprintf(file_path, "%s%s", dirpath, path);

  res = truncate(file_path, size);
  if (res == -1)
    return -errno;

  // tulis ke hayolongapain_d04.log
  hayolongapain_d04_log("TRUNCATE", file_path);

  return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
  int res;
  char file_path[maxSize];
  char *a = strstr(path, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);
  char *b = strstr(path, ian);
  if (b != NULL)
    vigenere_decrypt_func(b);

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(file_path, "%s", path);
  }
  else
    sprintf(file_path, "%s%s", dirpath, path);

  res = open(file_path, fi->flags);
  if (res == -1)
    return -errno;
  close(res);

  // tulis ke hayolongapain_d04.log
  hayolongapain_d04_log("OPEN", file_path);

  return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  int fd;
  int res;
  char file_path[maxSize];

  char *a = strstr(path, animeku);
  if (a != NULL)
    atbash_x_rot13_decrypt_func(a);

  char *b = strstr(path, ian);
  if (b != NULL)
    vigenere_decrypt_func(b);

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(file_path, "%s", path);
  }
  else
    sprintf(file_path, "%s%s", dirpath, path);

  (void)fi;
  fd = open(file_path, O_WRONLY);
  if (fd == -1)
    return -errno;

  res = pwrite(fd, buf, size, offset);
  if (res == -1)
    res = -errno;
  close(fd);

  // tulis ke hayolongapain_d04.log
  hayolongapain_d04_log("WRITE", file_path);

  return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,

    .chmod = xmp_chmod,
    .chown = xmp_chown,
    .truncate = xmp_truncate,
    .open = xmp_open,
    .write = xmp_write,

};

int main(int argc, char *argv[])
{
  char *username = getenv("USER");

  sprintf(dirpath, "/home/%s/Documents", username);

  umask(0);

  return fuse_main(argc, argv, &xmp_oper, NULL);
}
